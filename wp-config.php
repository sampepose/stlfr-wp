<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Heroku Postgres settings - from Heroku Environment ** //
$db = parse_url($_ENV["DATABASE_URL"]);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', trim($db["path"],"/"));

/** MySQL database username */
define('DB_USER', $db["user"]);

/** MySQL database password */
define('DB_PASSWORD', $db["pass"]);

/** MySQL hostname */
define('DB_HOST', $db["host"]);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EH4$~B6IOtb8-Km>|G1P^1s%RT/I~PP5}om[[E~R|-sxSX(b5xh0F{|x+au-c-Jd');
define('SECURE_AUTH_KEY',  'C-}&-6OPuxMxCu*O~Ggqxt+P7Q! x;4nnW_)GB[qXk7?^LOjtM;{K`npIw!)2Um!');
define('LOGGED_IN_KEY',    'c4jN@N`1pW-caL.mg<S@_U8sMYZ&DB);{|VRp%-ETXX_kf|+nhN}{~t.L*M{Oi(S');
define('NONCE_KEY',        '!aF<<LNP.A2*MDCBR50aJbBU)kEAV)etAv<}BI+$;nkfN##}f0C+~nOw!iH5{s!Z');
define('AUTH_SALT',        ']mMU[q;tjx<_BRrR1|{5_XAr6?GQTuyU*Dp2q#Dqf]Ic 6!A[T?jzr~:7LeR]Ah`');
define('SECURE_AUTH_SALT', 'C8QP+3:F&LecUhk!%ZIK2EFY;Sotdv<#{ D,;R*eh<1vS@rr,Qhg])b|{FcR_]9[');
define('LOGGED_IN_SALT',   ' z0Q-+q3V*s}K@-+zd1Q-#Q96/Di9 ~G?}x~YOVSoQxj5[3[RM_wH`3)(M)N.8+8');
define('NONCE_SALT',       'm{U=<| Pw6- @W_IBp/aZJ$-zFC%wb?:u#Nf0bi@r3`H6*sn.-yO:`Mu%zd-11Cw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
