<?php
if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
		'1' => 'First column',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title-1 block">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
		'2' => 'Second column',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title-1 block">',
        'after_title' => '</h3>',
    ));
}
?>