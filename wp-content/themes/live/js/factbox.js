var index = 0;
var facts = new Array(11);
facts[0] = "40% of food produced in America is wasted";
facts[1] = "16 million American children currently live in poverty";
facts[2] = "50% of children in St. Louis city are in poverty";
facts[3] = "50 million Americans are on food stamps";
facts[4] = "Fewer than 20% of US nonprofits are involving youth in their work";
facts[5] = "Over 61% of American adults feel that teenagers face a crisis in their morals and values";
facts[6] = "Only 20% of young people feel that adults in the community value youth";
facts[7] = "1 in 3 people in St. Louis receives help from the United Way";
facts[8] = "50 million Americans live in poverty";
facts[9] = "1 in 7 people went hungry in 2011";
facts[10] = "15% of St. Louis residents rely on food pantries, food stamps or soup kitchens for daily meals";

$(document).ready(function () {
    index = Math.floor(Math.random() * facts.length);

    $("#fact").html(facts[index]);
    var height = $("#fact").css("height");
    $("#factspacer").css("height", height);
    index++;
    setTimeout(swapFact, 5000);
});

function swapFact() {
    $("#fact").fadeOut('slow', function () {
        $("#fact").css("display", "block").html(facts[index]).animate({opacity:1}, 'slow');
        index++;
        if (index >= facts.length)
            index = 0;
    });
    setTimeout(swapFact, 5000);
}