<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN">
<head>
<?php get_header(); ?>
</head>
<body>
	<div class="bg tac">
		<div class="main">
			<div class="header">
				<a href="<?php echo get_settings('home'); ?>/" title=""><?php bloginfo('name'); ?></a>
				<span class="slogan"><?php bloginfo('description'); ?></span>
			</div>
			<?php include (TEMPLATEPATH . "/blocks/navigation.php"); ?>
			<div class="left-edge">
				<div class="right-edge">
					<div class="bottom-edge">
						<div class="side-paddings">
							<div class="column-2 fl">
								<ul class="list-3">
								<?php if (have_posts()) : ?>
                                    <?php while (have_posts()) : the_post(); ?>
									<li>
										<div class="fr">
                                        	<h1><?php the_title(); ?></h1>
											<div class="list-title-info">Filed under <?php the_category(', ') ?></div>
                                            <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
										</div>
										<div class="date fl">
											<span><?php the_time('M') ?></span>
											<?php the_time('j') ?>
										</div>
										<div class="text clear">
											<?php the_content('Read the rest of this entry &raquo;'); ?>
										</div>
											
										<div class="list-title-info"><?php the_tags('Tagged as: ', ', ', '<br />'); ?></div>
											
										<div class="link-list">
                                            <?php comments_popup_link('No Comments', '1 Comment', '% Comments', 'ico-2'); ?>
											<!-- <a href="item.html" title="" class="ico-1">Permalink</a> -->
										</div>
									</li>
									<?php edit_post_link('Edit', '<p>', '</p>'); ?>
									<?php endwhile; ?>
                            
                                <?php else : ?>
                            	<li>
                                    <h2 class="center">Not Found</h2>
                                    <p class="center">Sorry, but you are looking for something that isn't here.</p>
                                    <?php include (TEMPLATEPATH . "/searchform.php"); ?>
                                </li>
                                <?php endif; ?>
								</ul>
                                <?php comments_template(); ?>
							</div>
                            <?php get_sidebar(); ?>
							<div class="clr">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
			<?php get_footer(); ?>
		</div>
	</div>
</body>
</html>