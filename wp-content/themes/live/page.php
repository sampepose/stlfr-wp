<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN">
<head>
<?php get_header(); ?>
</head>
<body>
	<div class="bg tac">
		<div class="main">
			<div class="header">
				<a class="head" href="<?php echo get_settings('home'); ?>/" title=""><?php bloginfo('name'); ?></a>
				<span class="slogan"><?php bloginfo('description'); ?></span>
				<a href="https://twitter.com/_STLFR" target="_twblank">
					<img src="http://stlfr-wordpress.s3.amazonaws.com/2013/05/twitter-icon.png" alt="Twitter" style="vertical-align: middle;display:block; float:right; padding-top:25px; padding-right:6px" height="30">
				</a>
				<a href="http://www.youtube.com/watch?v=hX51qyq5Xtk&amp;list=PL128AE484D2AF0AB3&amp;feature=view_all" target="_ytblank">
					<img src="http://stlfr-wordpress.s3.amazonaws.com/2013/05/youtube2.png" alt="Youtube" style="vertical-align: middle;display:block; float:right; padding-top:24px; padding-right:6px" height="30">
				</a>
				<a href="http://www.facebook.com/pages/St-Louis-Food-Rescue/164187767011687" target="_fbblank">
					<img src="http://stlfr-wordpress.s3.amazonaws.com/2013/05/facebook.png" alt="Facebook" style="vertical-align: middle;display:block; float:right; padding-top:24px; padding-right:5px" height="30">
				</a>
			</div>
			<?php include (TEMPLATEPATH . "/blocks/navigation.php"); ?>
			<div class="left-edge">
				<div class="right-edge">
					<div class="bottom-edge">
						<div class="side-paddings">
							<div id="factContainer">
                    						<div id="fact"></div>
                  					</div>
							<?php include (TEMPLATEPATH . '/sidebar-left.php'); ?>
							<div class="column-2 fl">
								<ul class="list-3">
								<?php if (have_posts()) : ?>
                                    <?php while (have_posts()) : the_post(); ?>
									<li>
										<div class="f;">
                                        	<h1><?php the_title(); ?></h1>
										</div>
										<div class="text clear">
											<?php the_content('Read the rest of this entry &raquo;'); ?>
										</div>
										<div class="link-list">
                                            
											<!-- <a href="item.html" title="" class="ico-1">Permalink</a> -->
										</div>
									</li>
									<?php edit_post_link('Edit', '<p>', '</p>'); ?>
									<?php endwhile; ?>
                            
                                <?php else : ?>
								<li>
                                    <h2 class="center">Not Found</h2>
                                    <p class="center">Sorry, but you are looking for something that isn't here.</p>
                                    <?php include (TEMPLATEPATH . "/searchform.php"); ?>
                                </li>
                                <?php endif; ?>
								</ul>
							</div>
                            <?php include (TEMPLATEPATH . '/sidebar-right.php'); ?>
							<div class="clr">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
			<div class="left-edge">
           			 <div class="right-edge">
              				<div class="bottom-edge">
                				<div style="height: 200px; margin-top: 10px;" class="footer">
                 					<div class="footerhoby">Much credit goes to<br><a href="http://www.hoby.org/" title="HOBY" target="_blank"><img src="http://stlfr-wordpress.s3.amazonaws.com/2013/05/HOBY.jpeg" alt="HOBY"></a><br>for inspiring this organization to be created.<br><br></div>
                  					<div style="margin-left: auto; margin-right: auto; text-align: center;">
                    						PO Box 523 &nbsp; | &nbsp; St. Louis, Mo.
                    							63040
                  					</div>
                				</div>
              				</div>
            			</div>
          		</div>
			<p class="smallText">Website created and maintained by Sam Pepose, sampepose@stlfoodrescue.org</p>
            </div>
          </div>
