<div class="column-1 fr">
  <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(2) ) : ?>

        <!--sidebox start -->
        <div>
            <h3 class="title-1 block"><?php _e('Archives'); ?></h3>
            <ul>
                <?php wp_get_archives('type=monthly'); ?>
            </ul>
        </div>
        <!--sidebox end -->
        
        <!--sidebox start -->
        <div>
            <h3 class="title-1 block">Meta</h3>
            <ul>
                <li class="rss"><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></li>
                <li class="rss"><a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a></li>
                <li class="wordpress"><a href="http://www.wordpress.org" title="Powered by WordPress">WordPress</a></li>
                <li class="login"><?php wp_loginout(); ?></li>
            </ul>
        </div>
        <!--sidebox end -->

  <?php endif; ?>
</div>
