<div class="column-1 fr column-left">
  <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(1) ) : ?>

        <!--sidebox start -->
        <div>
            <h3 class="title-1 block"><?php _e('Categories'); ?></h3>
            <ul>
                <?php wp_list_cats('sort_column=name&optioncount=1&hierarchical=0'); ?>
            </ul>
        </div>
        <!--sidebox end -->
        
        <!--sidebox start -->
		<div>
			<h3 class="title-1 block"><?php _e('Recent Articles'); ?></h3>
			<ul>
				<?php $posts = get_posts('numberposts=10'); foreach($posts as $post) : ?>
				<li><a href= "<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endforeach; ?>
			</ul>	
		</div>
        <!--sidebox end -->
        
        <!--sidebox start -->
        <div>
            <h3 class="title-1 block"><?php _e('Links'); ?></h3>
            <ul>
                <?php get_links('-1', '<li>', '</li>', '<br />', FALSE, 'id', FALSE, FALSE, -1, FALSE); ?>
            </ul>
        </div>
        <!--sidebox end -->
                
  <?php endif; ?>
</div>
